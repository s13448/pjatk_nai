"""
authors: Marcin Biedrzycki, Tomasz Sosiński
emails: s13448@pjwstk.edu.pl , s16216@pjwstk.edu.pl
task: Movies recomendation
parser in .net
"""


import json
import numpy as np
from compute_scores import euclidean_score

# Finds users in the dataset that are similar to the input user 
def find_similar_users(dataset, user, num_users):
    if user not in dataset:
        raise TypeError('Cannot find ' + user + ' in the dataset')

    # Compute Pearson score between input user 
    # and all the users in the dataset
    scores = np.array([[x, euclidean_score(dataset, user,
                                           x)] for x in dataset if x != user])

    # Sort the scores in decreasing order
    scores_sorted_desc = np.argsort(scores[:, 1])[::-1]
    scores_sorted_asc = np.argsort(scores[:, 1])[::1]

    # Extract the top 'num_users' scores
    top_users = scores_sorted_desc[:num_users]
    bottom_users = scores_sorted_asc[:num_users]
    scores_dictionary = {"top_users": scores[top_users], "bottom_users": scores[bottom_users]}
    return scores_dictionary


if __name__ == '__main__':

    # Initialize input for username as argument to command
    user = input("enter a username: ")

    # Load data from json file
    ratings_file = 'dataset.json'
    with open(ratings_file, 'r') as f:
        data = json.loads(f.read())

    print('\nUsers similar to ' + user + ':\n')
    result_users = find_similar_users(data, user, 5)

    # Set data for recommended and unrecommended
    similar_users = result_users["top_users"]
    unsimilar_users = result_users["bottom_users"]

    # Display recommended movies
    print('User\t\t\tSimilarity score euclidean')
    print('-' * 41)
    for item in similar_users:
        print(item[0], '\t\t', round(float(item[1]), 2))

    print('-' * 41)
    print('User\t\t\tUnsimilarity score euclidean')
    print('-' * 41)
    for item in unsimilar_users:
        print(item[0], '\t\t', round(float(item[1]), 2))

    movies_similar_user = data[similar_users[0][0]]
    movies_of_user = data[user]

    for item in movies_of_user:
        if item in movies_similar_user:
            movies_similar_user.pop(item)

    # Sort movies to get top 5 and bottom 5
    sorted_similar_user = sorted(movies_similar_user.items(), key=lambda x: x[1])

    # Set sorted top and bottom 5 movies 
    top_recommended = sorted_similar_user[-5:]
    bottom_recommended = sorted_similar_user[:5]

    print('-' * 41)

    # Display recommended movies
    print(f"Recommended Movies")
    for movie, rate in top_recommended:
        print(f"{movie} : {rate}")
    print('-' * 41)

    # Display unrecommended movies
    print(f"Not recommended Movies")
    for movie, rate in bottom_recommended:
        print(f"{movie} : {rate}")
    print('-' * 41)
