﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using MoreLinq;
using Newtonsoft.Json;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.TypeConverter;

namespace CsvParser
{
    
    class Program
    {
        static void Main(string[] args)
        {
            CsvParserOptions csvParserOptions = new CsvParserOptions(true, ',');
            var csvParser = new CsvParser<Movies>(csvParserOptions, new MyCsvMapping());
            var records = csvParser.ReadFromFile($@"../../../movies.csv", Encoding.UTF8);

            var movieswithNull = records.Select(x => x.Result).ToList();

            List<Movies> movies = new List<Movies>();
            foreach (var movie in movieswithNull)
            {
                if (movie is not null)
                {
                    movies.Add(movie);
                }
            }

            var moviesJson = new List<MoviesJson>();

            var users = movies
                .Where(x => x.User is not null)
                .DistinctBy(x => x.User)
                .Select(x => x.User)
                .ToList();

            foreach (var user in users)
            {
                var temp = new MoviesJson
                {
                    User = user,
                    Movies = new List<MovieJson>()
                };
                moviesJson.Add(temp);
            }

            foreach (var movie in movies)
            {
                var currentUserMovie = moviesJson.Where(x => x.User == movie.User).FirstOrDefault();
                currentUserMovie.Movies.Add(new MovieJson { Movie = movie.Movie, Rate = movie.Rate });
            }

            string json = JsonConvert.SerializeObject(moviesJson);

            File.WriteAllText($@"../../../data.json", json);
            
            Console.WriteLine("Hello World!");
        }
    }

    class MyCsvMapping : CsvMapping<Movies>
    {
        public MyCsvMapping() : base()
        {
            MapProperty(0, x => x.User);
            MapProperty(1, x => x.Movie);
            MapProperty(2, x => x.Rate);
        }
    }

    class Movies
    {
        public string User { get; set; }
        public string Movie { get; set; }
        public double Rate { get; set; }
    }

    class MoviesJson
    {
        public string User { get; set; }
        public List<MovieJson> Movies { get; set; }
    }

    class MovieJson
    {
        public string Movie { get; set; }
        public double Rate { get; set; }
    }
}